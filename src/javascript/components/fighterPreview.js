import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {

  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  let obj = "";
  for (const [key, value] of Object.entries(fighter)) {
    obj += `${key}: ${value}     \n`;
  }
  fighterElement.append(obj);
  fighterElement.append(createFighterImage(fighter));

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
