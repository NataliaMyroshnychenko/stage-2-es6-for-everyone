import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const arr = [firstFighter, secondFighter];
  const [attacker, defender] = [];

  return new Promise((resolve) => {
    let isBlockFirstFighter = false;
    let isBlockSecondFighter = false;

    let pressed = new Set();
    document.addEventListener('keydown', function (event) {
      pressed.add(event.code);

      switch (event.code) {
        case controls.PlayerOneAttack: //"KeyA"
          if (!isBlockSecondFighter) {
            const demage1to2 = getDamage(firstFighter, secondFighter);
            secondFighter.health = secondFighter.health - demage1to2;
          } else {
            isBlockSecondFighter = false;
          }
          break;
        case controls.PlayerTwoAttack: //"KeyJ"
          if (!isBlockFirstFighter) {
            const demage2to1 = getDamage(secondFighter, firstFighter);
            firstFighter.health = firstFighter.health - demage2to1;
          } else {
            isBlockFirstFighter = false;
          }
          break;
        case controls.PlayerOneBlock:  //'KeyD'
          isBlockFirstFighter = true;
          break;
        case controls.PlayerTwoBlock:  //'KeyL'
          isBlockSecondFighter = true;
          break;
        //функция обработки сочетания клавиш внизу добавлена, но не подключена в этот метод
      }
    });

    let promiseResolution = (secondFighter.health === 0)
      ? firstFighter : (firstFighter.health === 0)
        ? secondFighter : undefined;

    setTimeout(() => {
      (firstFighter ? resolve(firstFighter) : reject("Error"))
    });

    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  const result = (damage < 0)
    ? 0 : (damage > defender.health)
      ? defender.health : damage;

  setNewWidhtHealthBar(defender, result);

  return result;
}

export function getHitPower(fighter) {
  const attack = fighter.attack;
  const criticalHitChance = Math.floor(Math.random() * 2) + 1;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const defense = fighter.defense;
  const dodgeChance = Math.floor(Math.random() * 2) + 1;
  return defense * dodgeChance;
}

export function getDamageWithCriticalHit(attacker, defender) {
  const criticalHit = attacker.attack * 2;
  return criticalHit;
  // setTimeout(getDamageWithCriticalHit, 1000);
}

export function setNewWidhtHealthBar(fighter, damage) {

  //рассчитываем % урона и уменьшаем индикатор здоровья на урон
  const percentGamage = 100 * (fighter.health - damage) / fighter.health;

  var titte = document.querySelector('.fighter-preview___img').clientWidth;
  var titleFirstFighter = [...document.querySelectorAll('.fighter-preview___img')][0].title;

  var position = (titleFirstFighter == fighter.name) ? "left" : "right";
  var widthHealthBar = document.querySelector(`.arena___health-bar#${position}-fighter-indicator`).clientWidth;
  var changingHealthBar = widthHealthBar * percentGamage / 100;
  document.querySelector(`.arena___health-bar#${position}-fighter-indicator`).style.width = changingHealthBar + 'px';
}

export function checkCombinationKeys(func, ...controls) {
  let pressed = new Set();
  document.addEventListener('keydown', function (event) {
    pressed.add(event.code);
    for (let code of controls) { // все ли клавиши из набора нажаты?
      if (!pressed.has(elemArr)) {
        return;
      }
    }
    pressed.clear();
    func();

  });
}
